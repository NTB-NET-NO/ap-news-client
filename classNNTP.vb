Imports system.Net.Sockets
Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports System.Configuration
Imports APNewsService
Imports APNewsService.classXML
Imports APNewsService.classFile
Imports APNewsService.classTime

Public Class classNNTP
    ' -----------------------------------------------------------------------------
    ' NNTP
    '
    ' Description:
    '   This class does everything that has to do with receiving an article from the
    '   AP NNTP-news server. 
    '
    ' Parameters:
    '
    ' Returns:
    '
    ' Notes :
    '   none
    ' -----------------------------------------------------------------------------
    ' The default TCP/IP port number for NNTP is 119
    Public Port As Integer = 119
    Public Messages As Integer = 0
    Public errorCode As Integer = 0
    Public lastArticle As Integer = 0
    Public firstArticle As Integer = 0
    Public debugString As String = Nothing
    Public NNTPerrorCode As Integer = 0


    Private Const CommandFailure As String = "-ERR" ' This does not apply for NNTP-servers, but for emailservers.
    Private Const GroupFailure As String = "411" ' No such newsgroup
    Private Const CommandSuccess As String = "211" ' using this instead of an error... everything else are errors

    Private NNTPServer As TcpClient
    Private CommandSender As NetworkStream
    Private ContentReceiver As StreamReader

    Public Shared logFilePath As String = My.Settings.logFilePath

    Public Shared logFileName As String = logFilePath & "\" & APNewsService.datefilename & My.Settings.logFileName



    Public Function NNTPLastArticle(ByVal filename As String)
        ' -----------------------------------------------------------------------------
        ' NNTPLastArticle
        '
        ' Description:
        '   This function reads from the logfile related to the NNTP-group which only
        '   stores the last article fetched. 
        '
        ' Parameters:
        '   filename = logfile to read from.
        '
        ' Returns:
        '   The last article number written and sent to Notabene
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------


        Dim articleNum As String
        articleNum = ""


        If IO.File.Exists(filename) Then
            articleNum = IO.File.ReadAllText(filename)
        Else
            articleNum = 0
        End If

        If articleNum = vbCrLf Then articleNum = 0
        Return articleNum

    End Function
    Public Function getLastArticle(ByVal numArticle As Integer, _
        ByVal logfile As String, _
        ByVal start As Integer, _
        ByVal slutt As Integer, _
        ByVal antSaker As Integer)

        ' -----------------------------------------------------------------------------
        ' getLastArticle
        '
        ' Description:
        '   The purpose for this Function is to see if the program has
        '   been stopped for quite some time and therefor is out of sync
        '   when it comes to first article
        '   
        '
        ' Parameters:
        '   numArticle = The last article that we fetched
        '   logfile = the logfile to write to, to tell what is the lart article
        '   start = First article to fetch
        '   slutt = Last article to fetch
        '   antSaker = Number of articles to fetch
        ' 
        ' Returns:
        '   Number of article that is the last one...
        '
        ' Notes :
        '   In the first version we just wrote. In the new version of this function we are checking
        '   if there is a value in the file.
        '
        ' Revisions:
        '   26.10.07:   Changed so that it does not write to the logfile, but uses the class XML to write 
        '               to the XML-state file instead. This means that we have marked out the old call in
        '               APNewsService.getListMain()
        ' -----------------------------------------------------------------------------
        APNewsService.WriteLogFile("In getLastArticle")
        Dim numInFile As Integer = NNTPLastArticle(logfile)
        Dim XMLFile As New ClassXML

        If Not numInFile = slutt Then


            If numArticle = slutt Then

                ' Her lurer jeg p� om vi ikke skal skrive. Dette fordi den er lik. 
                XMLFile.WriteStateToXMLFile(logfile, numArticle)
                ' IO.File.WriteAllText(logfile, numArticle & vbCrLf)
            End If



            If numArticle = 0 Then
                numArticle = start
                XMLFile.WriteStateToXMLFile(logfile, numArticle)
                'IO.File.WriteAllText(logfile, numArticle & vbCrLf)
            End If

            If numArticle < start Then
                numArticle = start
                XMLFile.WriteStateToXMLFile(logfile, numArticle)
                'IO.File.WriteAllText(logfile, numArticle & vbCrLf)
            End If

            If slutt - numArticle > antSaker Then
                numArticle = slutt - antSaker
            End If
        End If

        Return numArticle
    End Function

    Public Sub writeLastArticle(ByVal numArticle As Integer, ByVal logfile As String)
        ' -----------------------------------------------------------------------------
        ' writeLastArticle
        '
        ' Description:
        '   This routine writes the number of the last article fetched to the
        '   logfile specified in the sub call.
        '
        ' Parameters:
        '   numArticle = Number of article that was the last to be fetched
        '   logfile = The article number to write.
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        IO.File.WriteAllText(logfile, numArticle & vbCrLf)
        Dim xmlgroup As String
        xmlgroup = logfile
        xmlgroup.Replace(".log", "")

        APNewsService.WriteLogFile("Skrevet " & numArticle & " til " & logfile)
    End Sub


    

    Public Sub getNumMessages(ByVal passWord As String, _
        ByVal groupName As String)
        ' -----------------------------------------------------------------------------
        ' getNumMessages
        '
        ' Description:
        '   This routine connects to the NNTP server and sends the GROUP command
        '   This returns a string that is splitted in order to get the number of
        '   the first article, the last article, the number of articles that is 
        '   available on the server.
        '   It also returns an OK code
        '   The line looks like this: 211 1234 3000234 3002322 group.name
        '
        ' Parameters:
        '   Password =  The password that needs to be used in order to get the 
        '               returnline
        '   groupName = Name of the group we are fething articles from
        '
        ' Returns:
        '   Messages = Number of messages that is available on the server
        '   firstArticle = Number on the first article (not necessary available)
        '   lastArticle = Number on the last article
        '   errorCode = the OK/Error-number. 211 means that it is OK. 
        '
        ' Notes :
        '   The number for the firstArticle is not necessary available. 
        ' -----------------------------------------------------------------------------

        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String
        Dim parts() As String

        Dim okCode As Integer
        Dim start As Integer
        Dim slutt As Integer
        Dim antSaker As Integer

        commandData = "GROUP " & groupName & ControlChars.CrLf
        contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
        CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
        responseString = ContentReceiver.ReadLine()

        commandData = "AUTHINFO PASS " & passWord & ControlChars.CrLf
        contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
        CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
        responseString = ContentReceiver.ReadLine()
        ' sjekker at vi ikke fikk en feilmelding. 
        If responseString.StartsWith("5") Then
            APNewsService.WriteLogFile(responseString)
            Throw New Exception(responseString)
            Exit Sub

        End If
        ' APNewsService.WriteLogFile("GROUP returned: " & responseString)
        debugString = responseString
        parts = Split(responseString, " ")

        If parts(0) = "211" Then
            If IsArray(parts) Then

                okCode = parts(0)
                antSaker = parts(1)
                start = parts(2)
                slutt = parts(3)
            End If
        End If

        Messages = Val(antSaker)
        firstArticle = Val(start)
        lastArticle = Val(slutt)
        errorCode = Val(okCode)
    End Sub
    Public Sub Connect(ByVal serverName As String, _
        ByVal userName As String, _
        ByVal passWord As String)
        ' -----------------------------------------------------------------------------
        ' Connect
        '
        ' Description:
        '   This routine connects to the NNTP-server using the servername, username 
        '   and password provided in the routine-call.
        '
        ' Parameters:
        '   serverName = The name of the NNTP-server. This is stored in app.config
        '   userName = The username of the user. This is stored in app.config
        '   passWord = the password of the user. This is stored in app.config
        '
        ' Returns:
        '   Nothing
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String = Nothing



        ' Connect to the NNTP-server
        Try
            NNTPServer = New TcpClient(serverName, Port)
            CommandSender = NNTPServer.GetStream()
            ContentReceiver = New StreamReader(CommandSender)

        Catch
            Throw
        End Try


        ' Drar igjennom brukernavn og passord
        If Not responseString = Nothing Then
            responseString = Nothing
        End If

        Try
            commandData = "AUTHINFO USER " & userName & ControlChars.CrLf
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
            responseString = ContentReceiver.ReadLine()
        Catch ex As System.Net.Sockets.SocketException
            APNewsService.WriteLogFile("***** CONNECTION ERROR *****")
            APNewsService.WriteErrorLogFile("Error happened in nntp.vb Connect. Connection failure", ex.Message)
            APNewsService.WriteErrorLogFile("Connection failure: ", ex.Message)
            APNewsService.WriteErrorLogFile("Socket errorcode: ", ex.SocketErrorCode.ToString)
            APNewsService.WriteErrorLogFile("Native errorcode: ", ex.NativeErrorCode.ToString)
            If Not ex.InnerException Is Nothing Then
                APNewsService.WriteErrorLogFile("Inner Exception Message: ", ex.InnerException.Message)
            End If

        Catch exs As Exception
            APNewsService.WriteErrorLogFile("Normal Exception connection failure (in nntp.vb Connect): ", exs.Message)
            If Not exs.InnerException Is Nothing Then
                APNewsService.WriteErrorLogFile("Inner Exception Message: ", exs.InnerException.Message)
            End If

        End Try


        ' sjekker at vi ikke fikk en feilmelding. 
        If responseString.StartsWith("5") Then
            APNewsService.WriteLogFile(responseString)
            Exit Sub
        End If



        If Not responseString = Nothing Then
            responseString = Nothing
        End If



        If Not responseString = Nothing Then
            responseString = Nothing

        End If
        commandData = "AUTHINFO PASS " & passWord & ControlChars.CrLf
        contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
        CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
        responseString = ContentReceiver.ReadLine()

        If responseString.StartsWith("5") Then
            APNewsService.WriteLogFile("***** ERROR I CONNECT *****")
            APNewsService.WriteLogFile(responseString)
            Throw New Exception(responseString)

        End If


    End Sub

    Public Sub Disconnect(ByVal passWord As String)
        ' -----------------------------------------------------------------------------
        ' Disconnect
        '
        ' Description:
        '   This subroutine disconnects from the NNTP-server.
        '
        ' Parameters:
        '   passWord = The password is needed in order to run the command... 
        '
        ' Returns:
        '   Nothing
        '   
        ' Notes :
        '   It looks like the password could be taken out of the routine.
        '   none
        ' -----------------------------------------------------------------------------
        ' Disconnects from the NNTP-server
        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String

        ' Tell the server that we are through
        APNewsService.WriteLogFile("Sender QUIT kommandoen til server")
        Try
            commandData = "QUIT" & ControlChars.CrLf
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
        Catch ex As System.Net.Sockets.SocketException
            APNewsService.WriteLogFile("***** ERROR: Disconnect *****")
            APNewsService.WriteLogFile("Error happened in nntp.vb Disconnect: Connection failure: " & ex.Message)
            APNewsService.WriteLogFile("Socket errorcode: " & ex.SocketErrorCode.ToString)
            APNewsService.WriteLogFile("Native errorcode: " & ex.NativeErrorCode.ToString)
            If Not ex.InnerException Is Nothing Then
                APNewsService.WriteLogFile("Inner Exception Message: " & ex.InnerException.Message)
            End If

        Catch exs As Exception
            APNewsService.WriteLogFile("Normal Exception connection failure (in nntp.vb Disconnect): " & exs.Message)
            If Not exs.InnerException Is Nothing Then
                APNewsService.WriteLogFile("Inner Exception Message: " & exs.InnerException.Message)
            End If

            Return
        End Try

        responseString = ContentReceiver.ReadLine()



        APNewsService.WriteLogFile(responseString)

        ContentReceiver.Close()
        CommandSender.Close()
        NNTPServer.Close()

        APNewsService.WriteLogFile("Sendt QUIT-kommando til serveren")

    End Sub

    Public Sub Close()
        ContentReceiver.Close()
        CommandSender.Close()
        NNTPServer.Close()
    End Sub


    Function GetMessage(ByVal whichMessage As Integer, ByVal password As String) As String
        ' -----------------------------------------------------------------------------
        ' GetMessage
        '
        ' Description:
        '   This function receives the NNTP-message asked for in the Function-call.
        '   Password has to be used to receive the message.
        '
        ' Parameters:
        '   whichMessage = number on the article to be received.
        '   password = password to be used to fetch the article. This is stored in app.config
        '
        ' Returns:
        '   Returns the whole message text.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim commandData As String
        Dim contentBuffer() As Byte
        Dim responseString As String

        Dim theMessage As New System.Text.StringBuilder
        Dim oneLine As String

        responseString = Nothing
        ' check for an invalid message
        ' Not adding this now

        Try
            ' Request the message
            If APNewsService.doDebug = True Then
                APNewsService.WriteLogFile("Henter: " & whichMessage.ToString)
            End If
            commandData = "ARTICLE " & whichMessage & ControlChars.CrLf
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
            responseString = ContentReceiver.ReadLine()

            If Not responseString.StartsWith("2") Then
                APNewsService.WriteLogFile(responseString)
                Throw New Exception(responseString)
            End If

            commandData = "AUTHINFO PASS " & password & ControlChars.CrLf
            contentBuffer = System.Text.Encoding.ASCII.GetBytes(commandData.ToCharArray())
            CommandSender.Write(contentBuffer, 0, contentBuffer.Length)
            responseString = ContentReceiver.ReadLine()

            ' All messages in NNTP ends with a single . so we go and search for that in the while-loop below. 
            If Not (responseString.StartsWith("4")) Then
                Do While (ContentReceiver.EndOfStream = False)
                    oneLine = ContentReceiver.ReadLine()
                    If (oneLine = ".") Then Exit Do
                    theMessage.AppendLine(oneLine)
                Loop
            Else
                ' Dette m� vi legge til i logfil. 
                APNewsService.WriteLogFile(responseString)
                Return responseString
            End If

        Catch ex As Exception
            APNewsService.WriteLogFile("***** ERROR: GetMessage *****")
            APNewsService.WriteErrorLogFile("Error happened in nntp.vb GetMessage. Message retrieval failed: ", ex.Message)
            APNewsService.WriteErrorLogFile("Inner exception:", ex.InnerException.Message)



        End Try

        ' return message
        Return theMessage.ToString

    End Function

End Class
