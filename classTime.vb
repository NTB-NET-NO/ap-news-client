Public Class classTime
    ' -----------------------------------------------------------------------------
    ' classTime
    '
    ' Description:
    '   This class does everything that has to do with time stuff. This is used to check
    '   for old files and if there is a difference between the time now and the creation of time
    '
    ' Parameters:
    '
    ' Returns:
    '
    ' Notes :
    '   none
    ' -----------------------------------------------------------------------------

    Public Function TimeDiff(ByVal Time1 As String, ByVal Time2 As String) As String
        ' -----------------------------------------------------------------------------
        ' TimeDiff
        '
        ' Description:
        '   This function tells how long time it has been since last time
        '
        ' Parameters:
        '   Time1 = The time of the creation
        '   Time2 = the now time... I presume......
        '
        ' Returns:
        '   The difference between the two values
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim MinsDiff As String
        Dim TheHours As String
        MinsDiff = DateDiff("n", Time1, Time2)
        'If midnight is between times
        MinsDiff = IIf(MinsDiff < 0, MinsDiff + 1440, MinsDiff)
        TheHours = Format(Int(MinsDiff / 60), "00")
        MinsDiff = Format(MinsDiff Mod 60, "00")
        TimeDiff = TheHours & ":" & MinsDiff
    End Function
End Class
