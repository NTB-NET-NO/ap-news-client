<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.APNewsServiceInstaller = New System.ServiceProcess.ServiceProcessInstaller
        Me.AP_NewsServiceInstaller = New System.ServiceProcess.ServiceInstaller
        Me.EventLogInstaller1 = New System.Diagnostics.EventLogInstaller
        '
        'APNewsServiceInstaller
        '
        Me.APNewsServiceInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.APNewsServiceInstaller.Password = Nothing
        Me.APNewsServiceInstaller.Username = Nothing
        '
        'AP_NewsServiceInstaller
        '
        Me.AP_NewsServiceInstaller.Description = "Service for � hente meldinger fra AP"
        Me.AP_NewsServiceInstaller.DisplayName = "AP News Service"
        Me.AP_NewsServiceInstaller.ServiceName = "APNewsService"
        '
        'EventLogInstaller1
        '
        Me.EventLogInstaller1.CategoryCount = 0
        Me.EventLogInstaller1.CategoryResourceFile = Nothing
        Me.EventLogInstaller1.Log = "Application"
        Me.EventLogInstaller1.MessageResourceFile = Nothing
        Me.EventLogInstaller1.ParameterResourceFile = Nothing
        Me.EventLogInstaller1.Source = "AP News Service"
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.APNewsServiceInstaller, Me.AP_NewsServiceInstaller, Me.EventLogInstaller1})

    End Sub
    Friend WithEvents APNewsServiceInstaller As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents AP_NewsServiceInstaller As System.ServiceProcess.ServiceInstaller
    Friend WithEvents EventLogInstaller1 As System.Diagnostics.EventLogInstaller

End Class
