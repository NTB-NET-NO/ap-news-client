'***********************************************************************************
'* AP-NNTP-Receiver.exe
'*
'* This application is getting news messages from the AP News Service
'*
'* USES:
'*		logFile2.vb:    Class functions to write to logfiles
'*      nntp.vb:        Class functions to connect to nntp-server and receive messages
'*      attachment.vb:  Class functions to save attachments in the message
'*      app.config:     Config-file for the application
'*      timer.vb:       Class functions to time the application (not sure if it is in use)
'*      settings.vb:    Settings for the application. No extra code in file, but this creates app.config
'*
'*
'* NOTES:
'*		All code is copied to the NNTP-AP-Receiver service
'*
'* CREATED BY: Trond Hus�, NTB
'* CREATED DATE: 2007.08.01
'* REVISION HISTORY:
'*      2007.10.15: Changed the way to get information out of the app.config-file. This is now saved in Receiver Controller while reading 
'*		
'*
'**********************************************************************************

Imports System.ServiceProcess
Imports System
Imports System.IO
Imports system.Net.Sockets
Imports system.text
Imports System.Net
Imports System.Collections.Specialized
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Timers
Imports ntb_FuncLib
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Diagnostics


Public Class APNewsService


    Inherits System.ServiceProcess.ServiceBase

    ' Henter variabler som skal brukes 
    Public Shared logFilePath As String = My.Settings.logFilePath '("logFilePath")
    Public Shared lFileName As String = My.Settings.logFileName ' ("logFileName")
    Private Shared today As Date = Now
    Private Shared yearnow As Integer
    Private Shared monthnow As Integer
    Private Shared daynow As Integer
    Private Shared timenow As Integer
    Public Shared datefilename As String
    Public Shared lastArticleNum As Integer
    Public Shared lastArticleContainer As ArrayList
    Public Shared lastArticleGroup As ArrayList
    Public Shared myLog
    Public Shared myLogType


    Public Shared logFileName As String
    Public Shared OutPath As String = My.Settings.OutPath
    Public Shared HomePath As String = My.Settings.APHomeDir
    Public Shared pollInterval As Integer = My.Settings.PollingInterval
    Public Shared NewsServer As String = My.Settings.APNewsServer ' ("APNewsServer")
    Public Shared dirRoot As String = My.Settings.APRoot '("APRoot")
    Public Shared RestartInterval As Integer = My.Settings.Restart

    Public doDisconnect As Boolean = False

    Private strDayTime As DateTime

    Private NNTPConnection As classNNTP = Nothing
    Private NNTPAttachment As Attachment = Nothing

    Private XMLfile As ClassXML = Nothing

    Private uName As String
    Private pWord As String
    Private apGroups As New System.Collections.ArrayList()

    Private contentReceiver As StreamReader
    Private NewsGroups As String = Nothing


    Private xmlConfig As New XmlDocument

    ' To get Configinformation
    Public filepath As String
    Public filename As String
    Public groups As New System.Collections.ArrayList
    Public FilesDelete As Integer

    Public Shared xmlStateFile As String = My.Settings.APStateFile
    Public Shared xmlStateFileExists As Boolean = False

    Public Shared appPath As String = My.Application.Info.DirectoryPath

    ' Variables related to errors
    Public Shared ErrorPath As String = My.Settings.ErrorPath
    Public Shared ErrorFileName As String

    Public Shared FromStart As Boolean = False

    ' Debug true/false
    Public Shared doDebug As Boolean = My.Settings.debug





    Public Shared Function GetDate()
        ' -----------------------------------------------------------------------------
        ' GetDate
        '
        ' Description:
        '   This subroutine Gets todays date and returns it
        '   
        '
        ' Parameters:
        '
        ' Returns:
        '   todays date
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim returnDate As String = Nothing
        today = Now
        yearnow = today.Year
        monthnow = today.Month
        daynow = today.Day

        Dim minutes As String = today.ToString("mm")

        
        ' timenow = Convert.ToString(today.AddMinutes(6))
        returnDate = yearnow.ToString & monthnow.ToString & daynow.ToString

        Return returnDate
    End Function

    Public Shared Function CreateLogFileName()
        ' -----------------------------------------------------------------------------
        ' CreateLogFileName
        '
        ' Description:
        '   This subroutine creates the logfile-filename.
        '   
        '
        ' Parameters:
        '
        ' Returns:
        '   Returns the logfilename
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        datefilename = Nothing
        datefilename = GetDate.ToString
        CreateLogFileName = logFilePath & "\" & datefilename & lFileName

        Return CreateLogFileName
    End Function

    Public Shared Function CreateErrorLogFileName()
        ' -----------------------------------------------------------------------------
        ' CreateErrorLogFileName
        '
        ' Description:
        '   This subroutine creates the error logfile name
        '   
        '
        ' Parameters:
        '
        ' Returns:
        '   The error log file name
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim strDate As String = GetDate.ToString
        CreateErrorLogFileName = ErrorPath & "\" & "error-" & strDate & ".log"

        Return CreateErrorLogFileName
    End Function
    Public Sub New()
        Trace.Write("AP NEWS service, NEW!")
        ' MyBase.New()
        init()
        Dim logFileName As String = Nothing
        logFileName = CreateLogFileName()
        Trace.Write("AP NEWS service, after CreateLogFileName!")
        Dim errorLogFile As String = Nothing
        errorLogFile = CreateErrorLogFileName()
        Trace.Write("AP NEWS service, after CreateErrorLogFileName!")
        Dim strMessage As String = "I New og starter initialize component!"

        ' This call is required by the Windows Form Designer.
        If File.Exists(logFileName) Then
            Trace.Write(logFileName.ToString)
            WriteLogFile(strMessage)
        Else
            Trace.Write(strMessage)
        End If

        Try
            InitializeComponent()
        Catch ext As TypeInitializationException
            Trace.Write("Initialize Type failed: " & ext.Message.ToString)

        Catch ex As Exception
            If File.Exists(errorlogfile) Then
                WriteErrorLogFile("Initialize component failed:", ex.ToString)
            Else
                Trace.Write("Initialize component failed:" & ex.ToString)
            End If


        End Try

        Try
            PollTimer.Start()
        Catch ext As TypeInitializationException
            Trace.Write("Initialize Type failed: " & ext.Message.ToString)

        Catch ex As Exception
            If File.Exists(errorLogFile) Then
                WriteErrorLogFile("PollTimer Start failed:", ex.ToString)
            Else
                Trace.Write("PollTimer Start failed: " & ex.ToString)
            End If


        End Try






    End Sub
    Public Shared Sub WriteErrorLogFile(ByVal strMessage As String, ByVal strException As String)
        ' -----------------------------------------------------------------------------
        ' WriteErrorLogFile
        '
        ' Description:
        '   This subroutine writes to the error logfile.
        '   
        '
        ' Parameters:
        '   strMessage = The message that is to be put in the error log file.
        '   strException = The Exeption the error trew. 
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        ErrorFileName = CreateErrorLogFileName()
        LogFile.WriteDebug(ErrorFileName, "Error: " & DateTime.Now())
        LogFile.WriteDebug(ErrorFileName, strMessage)
        LogFile.WriteDebug(ErrorFileName, strException)
    End Sub

    Public Shared Sub WriteLogFile(ByVal StrMessage As String)
        ' -----------------------------------------------------------------------------
        ' PollTimer_Elapsed
        '
        ' Description:
        '   
        '   
        '
        ' Parameters:
        '   
        '   
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        logFileName = Nothing
        logFileName = CreateLogFileName()
        LogFile.WriteDebug(logFileName, StrMessage)


    End Sub



    Private Sub PollTimer_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles PollTimer.Disposed
        ' -----------------------------------------------------------------------------
        ' PollTimer_Elapsed
        '
        ' Description:
        '   
        '   
        '
        ' Parameters:
        '   
        '   
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        If doDebug = True Then
            If File.Exists(logFileName) Then
                WriteLogFile("I PollTimer_Disposed.")
            Else
                EventLog1.WriteEntry("I PollTimer_Disposed")
            End If



        End If

    End Sub

    Public Sub PollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        ' -----------------------------------------------------------------------------
        ' PollTimer_Elapsed
        '
        ' Description:
        '   
        '   
        '
        ' Parameters:
        '   
        '   
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        'Stop events while handling files
        ' Lager ny logfil - dersom det trengs. 
        logFileName = Nothing
        logFileName = CreateLogFileName()


        If doDebug = True Then WriteLogFile("I PollTimer_Elapsed.")
        If doDebug = True Then WriteLogFile("Navnet p� logfile: " & logFileName)
        PollTimer.Enabled = False

        'Public Shared lastArticleNum As Integer
        'Public Shared lastArticleContainer As ArrayList
        'Public Shared lastArticleGroup As ArrayList
        'Do getting news messages
        If doDebug = True Then WriteLogFile("Henter nyhetsmeldinger.")
        Try
            getListMain()
        Catch ex As System.Net.Sockets.SocketException
            WriteErrorLogFile("Error happened in PollTimer_Elapsed during getListMain", ex.Message)
            WriteErrorLogFile("Socket errorcode: ", ex.SocketErrorCode.ToString)
            WriteErrorLogFile("Native errorcode: ", ex.NativeErrorCode.ToString)
            If Not ex.InnerException Is Nothing Then
                WriteErrorLogFile("Inner Exception Message: ", ex.InnerException.Message)
            End If
        Catch exs As Exception
            WriteErrorLogFile("Normal Exception connection failure: ", exs.Message)
            If Not exs.InnerException Is Nothing Then
                WriteErrorLogFile("Inner Exception Message: ", exs.InnerException.Message)
            End If
            NNTPConnection = Nothing

        End Try


        If doDebug = True Then WriteLogFile("Kobler oss av serveren")

        If NNTPConnection IsNot Nothing Then
            Try
                NNTPConnection.Disconnect(pWord)
            Catch ex As System.Net.Sockets.SocketException
                WriteErrorLogFile("Error happened in PollTimer_Elapsed NNTPConnection.Disconnect. Connection failure: ", ex.Message)
                WriteErrorLogFile("Socket errorcode: ", ex.SocketErrorCode.ToString)
                WriteErrorLogFile("Native errorcode: ", ex.NativeErrorCode.ToString)
                If Not ex.InnerException Is Nothing Then
                    WriteErrorLogFile("Inner Exception Message: ", ex.InnerException.Message)
                End If

            Catch exs As Exception
                WriteErrorLogFile("Normal Exception connection failure: ", exs.Message)
                If Not exs.InnerException Is Nothing Then
                    WriteErrorLogFile("Inner Exception Message: ", exs.InnerException.Message)
                End If


                Return
            End Try
        End If
        'Restart events

        PollTimer.Interval = pollInterval * 1000
        PollTimer.Enabled = True

        If doDebug = True Then WriteLogFile("Avslutter henting av nyhetsmeldinger.")

        


    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        ' 18.03.08: Flyttet init til �verst i rutinen slik at vi f�r laget logfile og slikt. 
        init()

        logFileName = Nothing
        logFileName = CreateLogFileName()

        FromStart = True

        PollTimer.Enabled = True

        WriteLogFile("EXE-filen ligger " & appPath)
        WriteLogFile("Kildekoden ligger: \\FilPrint\thu$\Visual Studio Projects\AP News Service New 2\bin\Release")
        WriteLogFile("In OnStart.")


        WriteLogFile("AP News Service Startet.")


    End Sub


    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        If doDebug = True Then LogFile.WriteLog(logFileName, "In OnStop.")

        PollTimer.Enabled = False
        LogFile.WriteLog(logFileName, "AP News Receiver stopped.")
    End Sub



    Protected Sub init()
        ' -----------------------------------------------------------------------------
        ' init
        '
        ' Description:
        '   This routine is called when the application starts. 
        '   It reads all the values from the app.config-file and stores it
        '   in strings, arrays and integers
        '   It also makes sure that the application is not connected to the News server
        '
        ' Parameters:
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------


        ' Starter med � lage mapper
        Trace.Write("AP NEWS service, INIT!")


        Try
            Trace.Write("Creating directories")
            CreateOutDirectories()
        Catch ex As Exception
            EventLog1.WriteEntry("Error: " & ex.ToString, EventLogEntryType.Error)
        End Try

        Trace.Write("We write something to log")
        WriteLogFile("heisann!")
        Trace.Write("We've written heisann to logfile!")

        If doDebug = True Then
            If IO.File.Exists(logFileName) = True Then
                WriteLogFile("Init AP NNTP Receiver")
            Else

                Trace.Write("Init AP NNTP Receiver - " & logFileName & " does not exists or cannot be created!")
            End If

        End If



        ' Variabler for � logge inn p� AP.
        '        Dim newApGroups As NameValueCollection

        Dim XMLfile As New ClassXML



        If doDebug = True Then
            WriteLogFile("Checking if " & appPath & "\" & xmlStateFile & " exists")
        End If

        Dim xmlStateFileExists As Boolean = False
        If (My.Computer.FileSystem.FileExists(appPath & "\" & xmlStateFile)) Then
            xmlStateFileExists = True
            XMLfile.readXML()
        Else
            WriteLogFile(appPath & "\" & xmlStateFile & " finnes ikke!")
            xmlStateFileExists = False

        End If

        If doDebug = True Then
            WriteLogFile("Done checking for file exists!")
        End If


        ' System.IO.DirectoryNotFoundException was unhandled
        ' Message: Could not find a part of the path 'c:\utvikling\APNewsReceiver\log\'.
        Dim numOfGroups As Integer = My.Settings.groups

        If doDebug = True Then
            WriteLogFile("Adding apGroups")
        End If
        apGroups.Clear()

        ' Her skal vi hente ut de som ikke er hardkodet i settingsfilen.
        ' Vi fors�ker � se om dette fungerer
        Dim agrps As StringCollection = My.Settings.APGroups
        Dim agrpc As Integer = agrps.Count

        Dim ac As Integer = 0
        For ac = 0 To agrpc - 1
            If doDebug = True Then
                WriteLogFile("Dette er det vi legger til: " & agrps.Item(ac).ToString)
            End If
            apGroups.Add(agrps.Item(ac).ToString)
        Next


        'apGroups.Add(My.Settings.group1)
        'apGroups.Add(My.Settings.group2)
        'apGroups.Add(My.Settings.group3)
        Dim initValues As ArrayList = Nothing

        If doDebug = True Then
            WriteLogFile("Finishing adding apGroups")
        End If


        If xmlStateFileExists = False Then
            If doDebug = True Then
                WriteLogFile("Starter WriteStateXMLFile existerte ikke, s� vi lager den")
            End If

            XMLfile.WriteStateXMLFile(apGroups)
            XMLfile.WriteStateToXMLFile(apGroups(0), "0")

            If doDebug = True Then
                WriteLogFile("Ferdig med WriteStateXMLFile")
            End If

        End If

        Dim a As Integer = apGroups.Count
        Dim b As Integer
        For b = 0 To a - 1

            If Not (My.Computer.FileSystem.DirectoryExists(OutPath & "\" & apGroups(b))) Then
                Directory.CreateDirectory(OutPath & "\" & apGroups(b))
                WriteLogFile("Created Newsgroup-directory: " & OutPath & "\" & apGroups(b))
            End If


            strDayTime = Now.ToString("hh:mm:ss")
            WriteLogFile("Added Newsgroup: " & apGroups(b))

        Next



        uName = My.Settings.APUserName
        pWord = My.Settings.APPassWord

        If doDebug = True Then
            WriteLogFile("Lagt til brukernavn og passord: " & uName & "/" & pWord)
        End If


        ' Dim apGroups() As String = {"ap.ds.dsf.all", "ap.european.news", "ap.headline.headlines.top"}
        Dim nntpGetTime As Integer = My.Settings.PollingInterval ' Sets receive to 30 seconds or whatever is in the config-file.
        Dim nntpErrorRetry As Integer = My.Settings.nntpErrorRetry ' sets receive to 10 seconds on error

        ' kobler oss ned for � v�re sikker p� at vi ikke er tilkoblet.
        If doDebug = True Then
            If doDebug = True Then WriteLogFile("Kobler fra serveren")
        End If

        If (NNTPConnection IsNot Nothing) Then
            Try
                NNTPConnection.Disconnect(pWord)
            Catch ex As System.Net.Sockets.SocketException
                WriteLogFile("Error happened in init - NNTPConnection.Disconnect")
                WriteLogFile("Connection failure: " & ex.Message)
                WriteLogFile("Socket errorcode: " & ex.SocketErrorCode.ToString)
                WriteLogFile("Native errorcode: " & ex.NativeErrorCode.ToString)
                If Not ex.InnerException Is Nothing Then
                    WriteLogFile("Inner Exception Message: " & ex.InnerException.Message)
                End If
            Catch exs As Exception
                WriteLogFile("Normal Exception connection failure: " & exs.Message & " In init")
                If Not exs.InnerException Is Nothing Then
                    WriteLogFile("Inner Exception Message: " & exs.InnerException.Message & " In Init")
                End If
                NNTPConnection = Nothing
                Return
            End Try
        End If

        If doDebug = True Then
            WriteLogFile("Done init")
        End If




    End Sub




    Sub CreateOutDirectories()
        ' -----------------------------------------------------------------------------
        ' CreateOutDirectories
        '
        ' Description:
        '   This routine creates directories. 
        '   This routine might be removed as we can use a function in logfile2.vb
        '   that does the same.
        '
        ' Parameters:
        '   HomePath = this creates the root-directory if it does not exists
        '   logFilePath = The path to the directory where the logfiles shall be placed
        '   OutPath = The directory where the XML-messages shall be stored
        '   ErrorPath = The directory where errormessages (number of the article that was not received is stored)
        '
        ' Returns:
        '   Nothing
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        If doDebug = True Then
            Trace.Write("In CreateOutDirectories. Starting creating directories!")
        End If

        Dim dirs() As String = {dirRoot, HomePath, logFilePath, OutPath, ErrorPath}
        Dim a As Integer

        For a = 0 To dirs.Length - 1

            If Not Directory.Exists(dirs(a)) Then
                Try
                    Directory.CreateDirectory(dirs(a))
                    Trace.Write("Creating: " & dirs(a).ToString)

                Catch dex As DirectoryNotFoundException
                    Trace.Write("Error while creating: " & dex.Message.ToString)

                End Try



                ' Vi kan ikke skrive til noe som ikke eksisterer.
                ' WriteLogFile("Created: " & dirs(a))
            Else
                Trace.Write(dirs(a) & " exists!")
            End If

        Next a
        If doDebug = True Then
            ' WriteLogFile("Ferdig med � lage directories")
        End If

        Trace.Write("In CreateOutDirectories. Done creating directories")

    End Sub

    Sub getListMain()
        ' -----------------------------------------------------------------------------
        ' getListMain
        '
        ' Description:
        '   This is the main routine in this application. It uses the different classes to
        '   connect, get number of articles, and so on.
        '   
        ' Parameters:
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim counter As Integer
        Dim content As String

        ' Henter lognavn igjen her fordi det kan tenkes at dagen skifter, og dermed skal jo ikke samme logfil brukes igjen.
        logFileName = Nothing
        logFileName = CreateLogFileName()
        WriteLogFile("Laget logfilnavn: " & logFileName)
        If doDebug = True Then
            WriteLogFile("Begynner hentingen av nyhetsmeldinger i getListMain")
        End If



        Dim XMLFile = New ClassXML
        Dim File As New classFile



        ' Setter opp en ny tcpClient
        Dim c As Integer

        Try
            If doDebug = True Then WriteLogFile("Fors�ker � koble til serveren")
            NNTPConnection = New classNNTP
            NNTPConnection.Connect(NewsServer, uName, pWord)


        Catch ex As System.Net.Sockets.SocketException
            WriteLogFile("Error happened in getListMain NNTPConnection.Connect")
            WriteLogFile("Connection failure: " & ex.Message)
            WriteLogFile("Socket errorcode: " & ex.SocketErrorCode.ToString)
            WriteLogFile("Native errorcode: " & ex.NativeErrorCode.ToString)
            If Not ex.InnerException Is Nothing Then
                WriteLogFile("Inner Exception Message: " & ex.InnerException.Message)
            End If

            If ex.Message.StartsWith("504") Then
                ' da skal det v�re overloaded og da er vi ikke koblet til. 
                doDisconnect = False
                Return
                Exit Sub
            End If

            ' If we get the 10060 (timeout) error then we restart the program.
            If ex.NativeErrorCode.ToString.StartsWith("1006") Then

                WriteLogFile("******* RESTARTING AFTER A CONNECTION ERROR *******")
                ' Marks out the line below since we aren't connected anyway.
                '        NNTPConnection.Disconnect(pWord)
                NNTPConnection = Nothing
                Call OnStop()
                Call OnStart(Nothing)
                Return
                Exit Sub


            End If



            If ex.SocketErrorCode.ToString = "TimedOut" Then
                ' da er vi ikke koblet til.
                NNTPConnection.Close()
                Return
                Exit Sub
            End If
            Return

        Catch exs As Exception
            WriteLogFile("Normal exeption failure: " & exs.Message)
            If Not exs.InnerException Is Nothing Then
                WriteLogFile("Inner Normal Exception Message: " & exs.InnerException.Message)
            End If
            NNTPConnection = Nothing
            Return



        End Try


        If doDebug = True Then WriteLogFile("Henter ut meldinger")
        Dim fileAge As String = Nothing
        If doDebug = True Then WriteLogFile("Antall meldinger: " & apGroups.Count.ToString)
        For c = 0 To apGroups.Count - 1
            ' Linjen under kan slettes ettersom vi ikke bruker denne logfilen lenger. 
            ' classNNTP.checkAgeOfLastArticle(logFilePath & "\" & apGroups(c) & ".log")
            Try
                If doDebug = True Then WriteLogFile("Sjekker alderen p� filen")
                fileAge = File.checkAgeofFile(appPath & "\" & xmlStateFile)
                If doDebug = True Then WriteLogFile("Dette er alderen: " & fileAge.ToString)
            Catch ex As Exception
                WriteLogFile(ex.ToString)
            End Try

            Dim fileTime As String = Right(fileAge, InStr(fileAge, ":") - 1)
            fileTime = fileTime.Replace(":", "")

            If fileTime < RestartInterval Then
                ' La oss sjekke hva som skjer dersom vi finner left
                Dim newfileTime As String = Left(fileAge, InStr(fileAge, ":"))
                newfileTime = newfileTime.Replace(":", "")
                If newfileTime <> 0 Then
                    fileTime = newfileTime
                End If
            End If



            If doDebug = True Then
                WriteLogFile("Dette er tiden: " & fileTime.ToString)
            End If


            If FromStart = False And fileTime >= RestartInterval Then
                WriteLogFile("******* RESTART OF SERVICE *******")
                WriteLogFile("Filen er eldre enn " & RestartInterval.ToString & " minutter gammel. Vi starter opp p� nytt!")
                NNTPConnection.Disconnect(pWord)
                NNTPConnection = Nothing
                Call OnStop()
                ' Da tar vi � lagrer en verdi til fila ogs� starter vi p� nytt
                ' Public Function readValueFromXML(ByVal GroupName As String)
                ' XMLFile.WriteStateToXMLFile(NewsGroups, counter)

                ' S� vi m� f�rst hente nummer fra nyhetsgruppe og s� telleren...
                WriteLogFile("******* Henter fra fil for � lagre f�r vi starter p� nytt *******")
                Dim lastArticle As String = XMLFile.readValueFromXML(apGroups(c).ToString)
                ' S� skriver vi til filen slik at den er 0 minutter gammel. 
                WriteLogFile("******* Skriver " & lastArticle.ToString & " til statefilen. *******")
                XMLFile.WriteStateToXMLFile(apGroups(c).ToString, lastArticle.ToString)
                ' Dim writeArticleLast As String = XMLFile.writestatetoxml(apGroups(c).ToString, lastArticle.ToString)
                WriteLogFile("******* Skrevet " & lastArticle.ToString & " til statefilen. *******")
                FromStart = True
                Call OnStart(Nothing)
                Return
                Exit Sub
            Else
                FromStart = False
            End If



            Try
                If doDebug Then
                    WriteLogFile("Fors�ker � finne ut hvor mange meldinger")
                End If
                NNTPConnection.getNumMessages(pWord, apGroups(c))
            Catch ex As System.Net.Sockets.SocketException
                WriteLogFile("Error happened in getListMain NNTPConnection.getNumMessages")
                WriteLogFile("Connection failure: " & ex.Message)
                WriteLogFile("Socket errorcode: " & ex.SocketErrorCode.ToString)
                WriteLogFile("Native errorcode: " & ex.NativeErrorCode.ToString)
                If Not ex.InnerException Is Nothing Then
                    WriteLogFile("Inner Exception Message: " & ex.InnerException.Message)
                End If
                Return
            Catch exs As Exception
                WriteLogFile("Error happened in getListMain NNTPConnection.getNumMessages (normal exception)")
                WriteLogFile("Normal Exception connection failure: " & exs.Message)
                If Not exs.InnerException Is Nothing Then
                    WriteLogFile("Inner Exception Message: " & exs.InnerException.Message)
                End If
                NNTPConnection = Nothing
                Return
            End Try


            NewsGroups = Nothing
            NewsGroups = apGroups(c)


            If (NNTPConnection.Messages = 0) Then
                WriteLogFile("Ingen meldinger ble funnet.")

                Try
                    NNTPConnection.Disconnect(pWord)
                Catch ex As System.Net.Sockets.SocketException
                    WriteLogFile("Error happened in getListMain NNTPConnection.Messages")
                    WriteLogFile("Connection failure: " & ex.Message)
                    WriteLogFile("Socket errorcode: " & ex.SocketErrorCode.ToString)
                    WriteLogFile("Native errorcode: " & ex.NativeErrorCode.ToString)
                    If Not ex.InnerException Is Nothing Then
                        WriteLogFile("Inner Exception Message: " & ex.InnerException.Message)
                    End If
                    Return
                Catch exs As Exception
                    WriteLogFile("Error happened in getListMain NNTPConnection.Messages (normal exception)")
                    WriteLogFile("Normal Exception connection failure: " & exs.Message)
                    If Not exs.InnerException Is Nothing Then
                        WriteLogFile("Inner Exception Message: " & exs.InnerException.Message)
                    End If
                    NNTPConnection = Nothing
                    Return
                End Try
                NNTPConnection = Nothing
                Return
            End If


            ' Get the messages 
            content = Nothing
            ' logFilePath & "\" & apGroups(c) & ".log"
            Dim fa As Integer = 0 ' first article
            Dim la As Integer = 0 ' last article 
            Dim am As Integer = 0
            Dim na As Integer = 0
            Dim l As Integer = 0
            Dim db As String = Nothing
            fa = Val(NNTPConnection.firstArticle)
            la = Val(NNTPConnection.lastArticle)

            am = Val(NNTPConnection.Messages)
            If doDebug = True Then
                WriteLogFile(fa & " er f�rste artikkel, " & la & " er siste artikkel")
            End If


            ' The line belov is marked out because we don't use the logfile anymore. 
            ' na = Val(NNTPConnection.NNTPLastArticle(logFilePath & "\" & NewsGroups & ".log"))

            na = Val(NNTPConnection.NNTPLastArticle(NewsGroups))

            ' Denne kan vi fortsette � bruke
            ' l = NNTPConnection.getLastArticle(na, logFilePath & "\" & NewsGroups & ".log", fa, la, am)
            l = XMLFile.readValueFromXML(NewsGroups)

            If doDebug = True Then WriteLogFile("Dette er verdien av l: " & l)
            If doDebug = True Then WriteLogFile("Dette er verdien fra " & NewsGroups & ": " & XMLFile.readValueFromXML(NewsGroups))
            db = NNTPConnection.debugString

            If l < fa Then
                l = fa
            End If

            'Messages = Val(antSaker)
            'firstArticle = Val(start)
            'lastArticle = Val(slutt)

            If na < la Then
                For counter = l + 1 To la
                    content = NNTPConnection.GetMessage(counter, pWord)

                    If Not (content.StartsWith("4")) Then


                        NNTPAttachment = New Attachment
                        Dim txtString = NNTPAttachment.getAttachment(content)
                        If (NNTPAttachment.IsBase64String(txtString) = True) Then
                            txtString = NNTPAttachment.DecodeBase64(txtString)
                            NNTPAttachment.saveAttachment(txtString, counter, OutPath & "\" & NewsGroups, logFilePath & "\" & NewsGroups)
                            WriteLogFile("Henter artikkel: " & counter.ToString & ". Lagt til mappe: " & OutPath & "\" & NewsGroups)
                            ' Her m� vi ogs� skrive til log-fil for denne nyhetsgruppen
                            ' Slik at vi ikke starter fra f�rste igjen.
                            ' NNTPConnection.writeLastArticle(counter, logFilePath & "\" & NewsGroups & ".log")

                            If la <> l Then
                                ' WriteLogFile("Now I will write " & counter & " since it is larger than " & la & " ....")
                                XMLFile.WriteStateToXMLFile(NewsGroups, counter)
                            End If





                        End If

                        ' Debug.WriteLine(content)
                    End If
                Next
            End If



        Next

        If doDebug = True Then WriteLogFile("Done fetching messages!")

    End Sub

    Public Sub pause(ByVal pausetime As Integer)
        ' -----------------------------------------------------------------------------
        ' pause
        '
        ' Description:
        '   This routine is used to pause the number of seconds stated in the app.config file
        '
        ' Parameters:
        '   pausetime = The number of seconds the application should pause
        '
        ' Returns:
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        ' New pause routine since the old one stops at 00.00

        If doDebug = True Then WriteLogFile("Pause sub starts")


        Dim ticksBefore As Long
        Dim ticksAfter As Long
        Dim tickSeconds As Double

        ' time the user
        ticksBefore = Now.Ticks
        Do While (Now.Ticks - ticksBefore) / 10000000.0 < pausetime
            ' looping just to get a pause
        Loop
        ticksAfter = Now.Ticks

        tickSeconds = (ticksAfter - ticksBefore) / 10000000.0


        If doDebug = True Then WriteLogFile("Ventet i " & tickSeconds.ToString & " sekunder")

    End Sub

End Class
