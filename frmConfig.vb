Imports System
Imports System.ServiceProcess
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Windows.Forms
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports ntb_FuncLib
Imports system.diagnostics




Public Class frmAPSC
    Dim myController As ServiceController
    Dim sStatus As String
    Public eventLog As New EventLog


    ' To get Configinformation
    Public filepath As String
    Public filename As String
    Public groups As New System.Collections.ArrayList
    Public FilesDelete As Integer

    Private xmlConfig As New XmlDocument

    

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Not doing much here am I...
        Me.Hide()
        Me.Visible = False


        '        readXmlFile()
        addHoursToList()
    End Sub

    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call serviceStart()
    End Sub

    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call serviceStop()

    End Sub

    Private Function ServiceStatus(ByVal status As String)
        ServiceStatus = myController.ServiceName & " is in state " & status

    End Function

    Private Sub iconUpdate(ByVal iconTitle As String, ByVal iconText As String, ByVal iconWait As Integer)
        NotifyIcon1.BalloonTipTitle = iconTitle
        NotifyIcon1.BalloonTipText = iconText
        NotifyIcon1.ShowBalloonTip(iconWait)
    End Sub

    Private Sub serviceStart()
        iconUpdate("Starting Service", "We are starting the AP News Receiver Service. Please hold", 1000)
        Try
            myController.Start()
        Catch ex As Exception
            If myController.Status.ToString = "Running" Then

                eventlog.WriteEntry("Could not start service it is already running")
            End If
        End Try
    End Sub

    Private Sub serviceStop()
        If myController.CanStop Then
            iconUpdate("Stopping Service", "We are stopping the AP News Receiver Service. Please hold", 1000)
            Try
                myController.Stop()

            Catch ex As Exception
                If Not myController.Status.ToString = "Running" Then
                    eventLog.WriteEntry("Service cannot be stopped or the service is already stopped")
                End If
            End Try
        Else
            MsgBox("can't stop!")
        End If
    End Sub
    Private Sub serviceRestart()
        If myController.CanStop Then
            iconUpdate("Stopping Service", "We are stopping the AP News Receiver Service. Please hold", 1000)
            Try
                myController.Stop()
            Catch ex As Exception
                If Not myController.Status.ToString = "Running" Then
                    eventLog.WriteEntry("Service cannot be stopped or the service is already stopped")
                End If

            End Try

            iconUpdate("Restarting Service", "We are restarting the AP News Receiver Service. Please hold", 1000)

            Try
                myController.Start()

            Catch ex As Exception
                If myController.Status.ToString = "Running" Then
                    eventLog.WriteEntry("Service cannot be started or the service is already started")
                End If

            End Try


        End If
    End Sub
    Private Sub btnRestart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call serviceRestart()

    End Sub

    


    Private Sub StartToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartToolStripMenuItem.Click
        Call serviceStart()
    End Sub

    Private Sub StopToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StopToolStripMenuItem.Click
        Call serviceStop()
    End Sub

    Private Sub RestartToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RestartToolStripMenuItem.Click
        Call serviceRestart()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        ' do nothing at the moment. 
        If ContextMenuStrip1.Items(5).Text = "Show &configuration" Then
            Me.WindowState = FormWindowState.Normal
            Me.Visible = True
            ContextMenuStrip1.Items(5).Text = "&Hide Configuration"
        Else
            ContextMenuStrip1.Items(5).Text = "Show &configuration"
            Me.Visible = False
        End If



    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        myController = New ServiceController("AP News Service")



        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Sub addHoursToList()
        Dim i
        For i = 1 To 100
            listHours.Items.Add(i)
            If i = FilesDelete Then
                listHours.SelectedItem = i
            End If


        Next
    End Sub

    


    
 

    Private Sub chkRootDir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRootDir.CheckedChanged
        If chkRootDir.Checked = False Then
            txtRootDir.Enabled = False
            btnBrowse.Enabled = False
        Else
            txtRootDir.Enabled = True
            btnBrowse.Enabled = True
        End If
    End Sub

    
    Private Sub chkHours_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkHours.CheckedChanged
        If chkHours.Checked = False Then
            listHours.Enabled = False
        Else
            listHours.Enabled = True
        End If
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        ' Denne skal f�rst lagre informasjonen s� lukke

        formMinimize()

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        formMinimize()
    End Sub

    Private Sub formMinimize()
        Me.Visible = False
        ContextMenuStrip1.Items(5).Text = "Show &configuration"

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        dlgFolder.Description = "Which directory do you want to use?"
        If (dlgFolder.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            txtRootDir.Text = dlgFolder.SelectedPath
        End If


    End Sub

    Private Sub ExitToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem1.Click
        'Dim quitter As New StartApApp.StartApApp

        'quitter.stoppApp()

        Me.Close()


    End Sub
End Class
