<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAPSC
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAPSC))
        Me.ServiceController1 = New System.ServiceProcess.ServiceController
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StartToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StopToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.RestartToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.tabController = New System.Windows.Forms.TabControl
        Me.tabGroups = New System.Windows.Forms.TabPage
        Me.chkGroups = New System.Windows.Forms.CheckedListBox
        Me.tabServer = New System.Windows.Forms.TabPage
        Me.txtServerName = New System.Windows.Forms.TextBox
        Me.txtUsername = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.tabDirectory = New System.Windows.Forms.TabPage
        Me.listHours = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkHours = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.chkRootDir = New System.Windows.Forms.CheckBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.txtRootDir = New System.Windows.Forms.TextBox
        Me.btnApply = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOk = New System.Windows.Forms.Button
        Me.dlgFolder = New System.Windows.Forms.FolderBrowserDialog
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator
        Me.ExitToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ContextMenuStrip1.SuspendLayout()
        Me.tabController.SuspendLayout()
        Me.tabGroups.SuspendLayout()
        Me.tabServer.SuspendLayout()
        Me.tabDirectory.SuspendLayout()
        Me.SuspendLayout()
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.BalloonTipText = "Ap News Receiver Service Controller"
        Me.NotifyIcon1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "AP News Receiver"
        Me.NotifyIcon1.Visible = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StartToolStripMenuItem, Me.StopToolStripMenuItem, Me.ToolStripMenuItem1, Me.RestartToolStripMenuItem, Me.ToolStripMenuItem2, Me.ExitToolStripMenuItem, Me.ToolStripMenuItem4, Me.ExitToolStripMenuItem1})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(178, 154)
        '
        'StartToolStripMenuItem
        '
        Me.StartToolStripMenuItem.Name = "StartToolStripMenuItem"
        Me.StartToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.StartToolStripMenuItem.Text = "&Start"
        '
        'StopToolStripMenuItem
        '
        Me.StopToolStripMenuItem.Name = "StopToolStripMenuItem"
        Me.StopToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.StopToolStripMenuItem.Text = "S&top"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(174, 6)
        '
        'RestartToolStripMenuItem
        '
        Me.RestartToolStripMenuItem.Name = "RestartToolStripMenuItem"
        Me.RestartToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.RestartToolStripMenuItem.Text = "&Restart"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(174, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.ExitToolStripMenuItem.Text = "Show &configuration"
        '
        'tabController
        '
        Me.tabController.Controls.Add(Me.tabGroups)
        Me.tabController.Controls.Add(Me.tabServer)
        Me.tabController.Controls.Add(Me.tabDirectory)
        Me.tabController.Location = New System.Drawing.Point(12, 12)
        Me.tabController.Name = "tabController"
        Me.tabController.SelectedIndex = 0
        Me.tabController.Size = New System.Drawing.Size(470, 422)
        Me.tabController.TabIndex = 17
        '
        'tabGroups
        '
        Me.tabGroups.Controls.Add(Me.chkGroups)
        Me.tabGroups.Location = New System.Drawing.Point(4, 22)
        Me.tabGroups.Name = "tabGroups"
        Me.tabGroups.Padding = New System.Windows.Forms.Padding(3)
        Me.tabGroups.Size = New System.Drawing.Size(462, 396)
        Me.tabGroups.TabIndex = 0
        Me.tabGroups.Text = "Newsgroups"
        Me.tabGroups.UseVisualStyleBackColor = True
        '
        'chkGroups
        '
        Me.chkGroups.FormattingEnabled = True
        Me.chkGroups.Location = New System.Drawing.Point(6, 10)
        Me.chkGroups.Name = "chkGroups"
        Me.chkGroups.Size = New System.Drawing.Size(450, 379)
        Me.chkGroups.TabIndex = 0
        '
        'tabServer
        '
        Me.tabServer.Controls.Add(Me.txtServerName)
        Me.tabServer.Controls.Add(Me.txtUsername)
        Me.tabServer.Controls.Add(Me.Label6)
        Me.tabServer.Controls.Add(Me.Label5)
        Me.tabServer.Controls.Add(Me.Label7)
        Me.tabServer.Controls.Add(Me.txtPassword)
        Me.tabServer.Location = New System.Drawing.Point(4, 22)
        Me.tabServer.Name = "tabServer"
        Me.tabServer.Padding = New System.Windows.Forms.Padding(3)
        Me.tabServer.Size = New System.Drawing.Size(462, 396)
        Me.tabServer.TabIndex = 1
        Me.tabServer.Text = "Server settings"
        Me.tabServer.UseVisualStyleBackColor = True
        '
        'txtServerName
        '
        Me.txtServerName.Location = New System.Drawing.Point(197, 11)
        Me.txtServerName.Name = "txtServerName"
        Me.txtServerName.Size = New System.Drawing.Size(250, 20)
        Me.txtServerName.TabIndex = 14
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(197, 44)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(100, 20)
        Me.txtUsername.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 47)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Username"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Password"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Servername"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(197, 81)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(100, 20)
        Me.txtPassword.TabIndex = 16
        '
        'tabDirectory
        '
        Me.tabDirectory.Controls.Add(Me.listHours)
        Me.tabDirectory.Controls.Add(Me.Label2)
        Me.tabDirectory.Controls.Add(Me.Label1)
        Me.tabDirectory.Controls.Add(Me.chkHours)
        Me.tabDirectory.Controls.Add(Me.CheckBox2)
        Me.tabDirectory.Controls.Add(Me.chkRootDir)
        Me.tabDirectory.Controls.Add(Me.btnBrowse)
        Me.tabDirectory.Controls.Add(Me.txtRootDir)
        Me.tabDirectory.Location = New System.Drawing.Point(4, 22)
        Me.tabDirectory.Name = "tabDirectory"
        Me.tabDirectory.Size = New System.Drawing.Size(462, 396)
        Me.tabDirectory.TabIndex = 2
        Me.tabDirectory.Text = "Directory setting"
        Me.tabDirectory.UseVisualStyleBackColor = True
        '
        'listHours
        '
        Me.listHours.FormattingEnabled = True
        Me.listHours.Location = New System.Drawing.Point(316, 169)
        Me.listHours.Name = "listHours"
        Me.listHours.Size = New System.Drawing.Size(75, 17)
        Me.listHours.TabIndex = 26
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 98)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(416, 47)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = resources.GetString("Label2.Text")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(397, 170)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Hours"
        '
        'chkHours
        '
        Me.chkHours.AutoSize = True
        Me.chkHours.Checked = True
        Me.chkHours.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkHours.Location = New System.Drawing.Point(16, 169)
        Me.chkHours.Name = "chkHours"
        Me.chkHours.Size = New System.Drawing.Size(216, 17)
        Me.chkHours.TabIndex = 22
        Me.chkHours.Text = "Automatically expire downloaded articles"
        Me.chkHours.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(16, 78)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(285, 17)
        Me.CheckBox2.TabIndex = 21
        Me.CheckBox2.Text = "Create directory structure to mirror newsgroup hierarchy"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'chkRootDir
        '
        Me.chkRootDir.AutoSize = True
        Me.chkRootDir.Checked = True
        Me.chkRootDir.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRootDir.Location = New System.Drawing.Point(16, 18)
        Me.chkRootDir.Name = "chkRootDir"
        Me.chkRootDir.Size = New System.Drawing.Size(192, 17)
        Me.chkRootDir.TabIndex = 20
        Me.chkRootDir.Text = "Save downloaded articles to folder:"
        Me.chkRootDir.UseVisualStyleBackColor = True
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(357, 39)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse.TabIndex = 19
        Me.btnBrowse.Text = "&Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'txtRootDir
        '
        Me.txtRootDir.Location = New System.Drawing.Point(16, 41)
        Me.txtRootDir.Name = "txtRootDir"
        Me.txtRootDir.Size = New System.Drawing.Size(256, 20)
        Me.txtRootDir.TabIndex = 17
        '
        'btnApply
        '
        Me.btnApply.Location = New System.Drawing.Point(388, 441)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(75, 23)
        Me.btnApply.TabIndex = 18
        Me.btnApply.Text = "&Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(294, 441)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 19
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(213, 440)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 20
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(174, 6)
        '
        'ExitToolStripMenuItem1
        '
        Me.ExitToolStripMenuItem1.Name = "ExitToolStripMenuItem1"
        Me.ExitToolStripMenuItem1.Size = New System.Drawing.Size(177, 22)
        Me.ExitToolStripMenuItem1.Text = "&Exit"
        '
        'frmAPSC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(494, 476)
        Me.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.tabController)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(500, 500)
        Me.MinimumSize = New System.Drawing.Size(500, 500)
        Me.Name = "frmAPSC"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AP News Receiver Service Controller"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.tabController.ResumeLayout(False)
        Me.tabGroups.ResumeLayout(False)
        Me.tabServer.ResumeLayout(False)
        Me.tabServer.PerformLayout()
        Me.tabDirectory.ResumeLayout(False)
        Me.tabDirectory.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ServiceController1 As System.ServiceProcess.ServiceController
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents StartToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StopToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RestartToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tabController As System.Windows.Forms.TabControl
    Friend WithEvents tabGroups As System.Windows.Forms.TabPage
    Friend WithEvents tabServer As System.Windows.Forms.TabPage
    Friend WithEvents txtServerName As System.Windows.Forms.TextBox
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents tabDirectory As System.Windows.Forms.TabPage
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtRootDir As System.Windows.Forms.TextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkHours As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents chkRootDir As System.Windows.Forms.CheckBox
    Friend WithEvents chkGroups As System.Windows.Forms.CheckedListBox
    Friend WithEvents listHours As System.Windows.Forms.ListBox
    Friend WithEvents dlgFolder As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem

End Class
