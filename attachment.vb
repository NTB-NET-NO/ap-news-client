Imports System.Configuration.ConfigurationSettings
Imports System.Configuration
Imports APNewsService

Public Class Attachment
    ' -----------------------------------------------------------------------------
    ' Attachment
    '
    ' Description:
    '   This class handles everything that has to do with getting the 
    '   attachment out of the NNTP-news message. AP attaches their files in XML-format.
    '
    ' Parameters:
    '
    ' Returns:
    '
    ' Notes :
    '   none
    ' -----------------------------------------------------------------------------
    Public txtLength As Integer = 0
    Public txtContent As String = Nothing
    Public base64found As Boolean = False
    Public Shared logFilePath As String = My.Settings.logFilePath
    Public Shared logFileName As String = logFilePath & "\APReceiverLog.log"


    Public Function getAttachment(ByVal Content As String)
        ' -----------------------------------------------------------------------------
        ' getAttachment
        '
        ' Description:
        '   This Function loops through every line in the message, when it comes to the
        '   part that tells that the attachment starts here it starts to 
        '   save the content in a string.
        '
        ' Parameters:
        '   Content = The message that has been fetched from the newsserver.
        '
        ' Returns:
        '   txtContent = The part of the message that contains the attachment (base64 encoded)
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim arrLoop As Integer
        Dim arrAdd As Integer = 0

        Dim strTemp() As String = Nothing
        Dim strLine As String = Nothing
        txtContent = Nothing
        base64found = False
        strTemp = Split(Content, vbCrLf)
        For arrLoop = 0 To strTemp.Length - 1

            strLine = strTemp(arrLoop)
            If Strings.Right(strLine, 14) = "base64 encoded" And base64found = False Then
                base64found = True
                ' Stop
                If Len(strTemp(arrLoop + 1)) > 1 Then
                    arrLoop += 1
                    ' Debug.WriteLine(arrArticle(arrLoop))
                ElseIf Len(strTemp(arrLoop + 2)) > 1 Then
                    arrLoop += 2
                    ' Debug.WriteLine(arrArticle(arrLoop))
                End If
            End If

            ' Hvis vi har funnet base64 s� skal vi begynne � legge til i streng
            If base64found = True Then
                strLine = strTemp(arrLoop)
                ' Da vet vi at linjen to etter er full av gibberish
                If Len(strLine) > 1 Then

                    txtContent += strLine
                    ' Debug.WriteLine(txtGibberish)
                Else
                    'Stop
                    Exit For
                End If
            End If

            If Len(strLine) > 1 Then
                ' Debug.WriteLine(arrLoop & arrArticle(arrLoop))

            End If

        Next

        Return txtContent


    End Function

    Public Function IsBase64String(ByVal s As String) As Boolean
        ' -----------------------------------------------------------------------------
        ' IsBase64String
        '
        ' Description:
        '   This Function checks if the string provided in the functioncall is really
        '   a Base64 encoded message.
        '
        ' Parameters:
        '   s = String provided in the functioncall which shall be a Base64 encoded message.
        ' 
        ' Returns:
        '   true / false 
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        Dim baTemp() As Byte
        Try
            baTemp = Convert.FromBase64String(s)
        Catch ex As FormatException
            Return False
        Catch ex As ArgumentNullException
            Return False
        End Try
        Return Convert.ToBase64String(baTemp).Equals(s)
    End Function


    Public Function DecodeBase64(ByVal input As String) As String
        ' -----------------------------------------------------------------------------
        ' DecodeBase64
        '
        ' Description:
        '   This function decodes the Base64 encoded string provided in the Function call
        '
        ' Parameters:
        '   input = String containing the Bas464 encoded part of the message
        '
        ' Returns:
        '   Returns the decoded message which now should be a XML-formated string
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------

        Dim strBytes() As Byte = System.Convert.FromBase64String(input)
        Return System.Text.Encoding.UTF8.GetString(strBytes)
    End Function

    Public Sub saveAttachment(ByVal Content As String, _
        ByVal fileName As Integer, _
        ByVal OutPath As String, _
        ByVal logFileName As String)
        ' -----------------------------------------------------------------------------
        ' saveAttachment
        '
        ' Description:
        '   This routine saves the now decoded string to a file.
        '
        ' Parameters:
        '   Content = the decoded string that was fetched from the NNTP-message
        '   filename = the filename to write to: Number + .xml
        '   outpath = Write to the path provided in the routine call
        '   logFileName = Which logfile to write to.
        '
        ' Returns:
        '   Nothing
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        ' lagrer filen... 
        Try


            IO.File.WriteAllText(OutPath & "\" & fileName & ".xml", Content)
            LogFile.WriteDebug(logFileName & ".attachment.log", "Written: " & OutPath & "\" & fileName & ".xml")

            IO.File.WriteAllText(logFileName & ".log", fileName & vbCrLf)
        Catch ex As Exception
            ' Return ex.Message
            APNewsService.WriteLogFile(ex.Message & " in attachment.vb")
        End Try
        ' Return fileName & ".xml"

    End Sub
End Class
