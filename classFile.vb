Imports system
Imports System.IO
Imports System.Text
Imports APNewsService
Imports ntb_FuncLib

Public Class classFile
    ' -----------------------------------------------------------------------------
    ' ClassXML
    '
    ' Description:
    '   This class does everything that has to do with File stuff. Which is deleting messages
    '   that are older than 12 hours.
    '
    ' Functions/Subroutines:
    '   checkAgeOfLastArticle = This routine checks if the file is older than 30 minutes. If it is we shall restart the service
    '
    ' Returns:
    '   Groupname returns the value of the Groupname in the XML.
    '
    ' Notes :
    '   none
    ' -----------------------------------------------------------------------------

    Public Function checkAgeofFile(ByVal FileName As String)
        ' -----------------------------------------------------------------------------
        ' checkAgeOfLastArticle
        '
        ' Description:
        '   This routine checks if the file is older than 30 minutes. If it is we shall restart the service

        '
        ' Parameters:
        '   logfile = The Logfile to check
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        ' APNewsService.WriteLogFile("I checkAgeofFile " & FileName.ToString)
        Dim LastWriteTime As Date
        Dim RightNow As Date
        Dim TimeNow As DateTime

        Dim Time As New classTime
        APNewsService.WriteLogFile("I checkAgeofFile")
        Try
            LastWriteTime = IO.File.GetLastWriteTime(FileName.ToString).ToString
        Catch fileex As System.IO.FileLoadException
            APNewsService.WriteLogFile("***** FileLoadException *****")
            APNewsService.WriteLogFile(fileex.ToString)
        Catch ex As Exception
            APNewsService.WriteLogFile("***** LastWriteTime = IO.File.GetLastWriteTime(FileName.ToString).ToString *****")
            APNewsService.WriteLogFile(ex.ToString)
        End Try


        RightNow = Date.Now
        TimeNow = DateTime.Now

        Dim a As String = Nothing
        Try
            a = Time.TimeDiff(LastWriteTime, TimeNow)
        Catch ex As Exception
            APNewsService.WriteLogFile("***** Time.TIMEDIFF *****")
            APNewsService.WriteLogFile(a.ToString)
        End Try
        APNewsService.WriteLogFile("Ferdig med � sjekke alder")

        Return a


        ' Ettersom jeg ikke vet hva vi f�r ut, s� tar jeg og printer rett til logfil. S� endrer jeg denne remarken etterp�.
        'APNewsService.WriteLogFile("Dette var sist det ble skrevet til logfilen " & FileName.ToString & " " & LastWriteTime.ToString)
        'APNewsService.WriteLogFile("Dette er datoen n� " & RightNow.ToString)
        'APNewsService.WriteLogFile("Dette er forskjellen: " & a.ToString)
    End Function

    Public Sub checkFilesInDirectory(ByVal dirName As String)
        Dim diff As String
        APNewsService.WriteLogFile("In checkFilesInDirectory: " & dirName.ToString)
        Dim FileArray As ArrayList = Nothing

        For Each oneFile As String In _
            My.Computer.FileSystem.GetFiles(dirName.ToString & "\")
            APNewsService.WriteLogFile(oneFile.ToString)
            Try
                diff = GetAgeOfFile(oneFile.ToString)
                APNewsService.WriteLogFile("The diff is :" & diff)

            Catch ex As Exception
                APNewsService.WriteLogFile(ex.ToString)
            End Try

        Next

    End Sub


    Function GetAgeOfFile(ByVal FileName As String)
        ' -----------------------------------------------------------------------------
        ' checkAgeOfLastArticle
        '
        ' Description:
        '   This routine checks if the file is older than 30 minutes. If it is we shall restart the service

        '
        ' Parameters:
        '   logfile = The Logfile to check
        '
        ' Returns:
        '   Nothing.
        '
        ' Notes :
        '   none
        ' -----------------------------------------------------------------------------
        APNewsService.WriteLogFile("I GetAgeOfFile " & FileName.ToString)
        Dim LastWriteTime As Date
        Dim RightNow As Date
        Dim TimeNow As DateTime

        Dim Time As New classTime

        Try
            LastWriteTime = IO.File.GetLastWriteTime(FileName.ToString).ToString
        Catch fileex As System.IO.FileLoadException
            APNewsService.WriteLogFile("***** FileLoadException *****")
            APNewsService.WriteLogFile(fileex.ToString)
        Catch ex As Exception
            APNewsService.WriteLogFile("***** LastWriteTime = IO.File.GetLastWriteTime(FileName.ToString).ToString *****")
            APNewsService.WriteLogFile(ex.ToString)
        End Try


        RightNow = Date.Now
        TimeNow = DateTime.Now

        Dim a As String = Nothing
        Try
            a = Time.TimeDiff(LastWriteTime, TimeNow)
        Catch ex As Exception
            APNewsService.WriteLogFile("***** Time.TIMEDIFF *****")
            APNewsService.WriteLogFile(a.ToString)
        End Try

        APNewsService.WriteLogFile("I will return " & a & " for the file " & FileName.ToString)
        Return a

    End Function


End Class
